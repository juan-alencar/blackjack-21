#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <locale.h>
#include "Baralho.c"
#include "cabecalho.c"


//fun��es
int mostrar_carta(void);
void cabecalho(void);
void linha(void);
void linha2(void);
void menu_player(void);
void pc_wins(void);
void player_wins(void);
void empate(void);

int main() {
	setlocale(LC_ALL, "portuguese");
	
	char opjogo;										//Variaveis que armazenam as opcoes dos jogadores em relacao ao jogo.
	char nome_jogador1, nome_jogador2;
	
	int sorte_pc;                                       // probabilidade do computador fazer outra jogada
	int jogada, totalj = 0, totalc = 0; 				// caso seja Jogador x Computador
	int total1 = 0, total2 = 0;							// caso seja Jogador x Jogador
    
	do{
        cabecalho();									//puxa a fun��o cabe�alho, que pinta o nome blackjack.
        menu_player();									// mostra o menu de 1 , 2 jogadores , Creditos , Regras e Sair
        scanf ("%c", &opjogo);  						// scanf que recebe o total de jogadores (max 2)
        getchar();
        
        
        switch(opjogo){
        	
            case '1':                                   //condi��o 1 (JOGADOR X COMPUTADOR)
                //Vez do usuario.
                opjogo = 'S'; 											    // inicializando a variavel com a condi��o verdadeira do while
                
				system("cls");											    //limpando o cmd para ficar bonito
                cabecalho();
                printf("\t\t\t\tVoc� tem que fazer o mais pr�ximo de 21 pontos para ganhar...\n");
                printf("\t\t\t\t\t   Por�m n�o pode passar desse limite\n");
                printf("\t\t\t\t\t    Voc� pode parar ap�s cada jogada\n\n\n");
                
				totalj = 0;												    // inicializando com 0 a variavel que receber� os valores do JOGADOR
                
				while ((opjogo == 's') || (opjogo == 'S')) 					//la�o que pergunta se o usuario quer fazer a jogada ou nao.
				{ 					
                    printf("\t\t\t\t\t       Deseja fazer a jogada?[S/N]\n");
                    scanf("%c", &opjogo); 								    //recebe se o usuario quer fazer a jogada, ou n�o
                    getchar();
                    printf("\n");
                    if ((opjogo == 'n') || (opjogo == 'N')){				//se for um "n/N", o programa para. Se ele quiser continuar...
						break;
                    }
                    
					jogada = mostrar_carta();
                    
					totalj += jogada; 									    // o total do jogador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
                    
					printf("\t\t\t\t\t\t      PONTOS: %d", totalj);
                    linha(); 											    // printa uma linha de "#" (funcao do arquivo 'cabecalho.c')
                    
					if (totalj >= 21) {									    //se o valor passar dos 21, ele n�o pode pedir mais cartas.
                        break;
                    }
                }
                
				printf("\t\t\t\t\t\tVOC� MARCOU %d PONTOS", totalj);        // mostra o total que o jogador conseguiu juntar e continua pra vez do COMPUTADOR
                linha();
                
				
				//Vez do Computador
                printf("\t\t\t\t\t\t  VEZ DO COMPUTADOR\n\n");
                printf("\t\t\t\t\t      Agora � minha vez de jogar\n\t\t\t\t\t      Vejo que voc� fez %d pontos\n\n", totalj);
                totalc = 0; 											    //inicializando com 0 a vari�vel que guardar� o total do computador
                
				 while (totalc < 21) 
				 {
                    srand(time(NULL));									    //gera numeros aleatorios de 1 ate 10 de acordo com o tempo do pc
                    jogada = (rand() % 12) +1; 							    //guarda na variavel jogada, um numero entre (0 - 9) + 1;
                    totalc += jogada; 									    //o total do computador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
                    
					if(totalc > totalj || totalc >= 21){
                        break;
                    } else if (totalc == 15) {                              //probabilidade do computador continuar jogando de acordo com sua pontua��o
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 2 de acordo com o tempo do pc
                        sorte_pc = (rand() % 2) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    } else if (totalc == 16) {
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 3 de acordo com o tempo do pc
                        sorte_pc = (rand() % 3) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    } else if (totalc == 17) {
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 5 de acordo com o tempo do pc
                        sorte_pc = (rand() % 5) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    } else if (totalc == 18) {
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 10 de acordo com o tempo do pc
                        sorte_pc = (rand() % 10) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    } else if (totalc == 19) {
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 20 de acordo com o tempo do pc
                        sorte_pc = (rand() % 20) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    } else if (totalc == 20) {
                        srand(time(NULL));								    //gera numeros aleatorios de 1 ate 30 de acordo com o tempo do pc
                        sorte_pc = (rand() % 30) +1;
                        if (sorte_pc != 1) {
                            break;
                        }
                    }
                    
                    printf("\t\t\t  Tirei %d e pretendo continuar jogando, pois ainda estou com %d pontos.\n", jogada, totalc); //mostra a carta e o total que ele juntou at� entao
                    sleep(1); 											    //faz o sistema parar por 1 segundo, at� a prox repeti��o do while (para a fun��o srand funcionar)
                }
                
                printf("\t\t\t\t\t Tirei %d pontos e fiquei com %d pontos.\n\n\n", jogada, totalc); //mostra a ultima jogada do PC, e o total que ele ficou
                printf("\t\t\t\t\t      O COMPUTADOR MARCOU %d PONTOS\n", totalc);
				linha();
                
				printf("\t\t\t\t\t\t\tPLACAR\n");
                printf("\t\t\t\t\t     JOGADOR  |%d| x |%d|  COMPUTADOR", totalj, totalc);
				linha();

                //Condi��es para final do jogo
                
				if (totalc == totalj) {  									//se os dois placares for iguais
                    printf("\t\t\t\t\t\tHouve um empate...\n\n");
                    empate();
                } else if (totalj == 21) { 									//se o jogador fez 21 pontos exatos e o pc nao
                    printf("\t\t\t\tO JOGADOR GANHOU, fazendo os gloriosos 21 pontos...\n");
                    player_wins();
                } else if (totalc == 21) {							 		//se o pc fez 21 pontos exatos e o jogador nao
                    printf("\t\t\t\tO COMPUTADOR GANHOU, fazendo os gloriosos 21 pontos...\n");
                    pc_wins();
                } else if ((totalc > 21) && (totalj > 21)) { 				//se o pc e o jogador ultrapassaram os 21 pontos
                    printf("\t\t\t\t    O computador e o jogador perdem por terem um n�mero maior de pontos do que � permitido...\n");
                } else if (totalc > 21 && totalj < 21) { 					//se o pc ultrapassou os 21 pontos, e o jogador n�o ultrapassou
                    printf("\t\t\t   O computador tem um n�mero maior de pontos do que � permitido...\n\t\t\t\t\t\tO JOGADOR VENCE\n");
                    player_wins();
                } else if (totalj > 21 && totalc < 21) { 					//se o jogador ultrapassou os 21 pontos, e o pc n�o ultrapassou
                    printf("\t\t\t   O jogador tem um n�mero maior de pontos do que � permitido...\nt\t\t\t\t\t\tO COMPUTADOR VENCE\n");
                    pc_wins();
                } else if (totalc > totalj) { 					            //se o pc tem mais pontos que o jogador
                    printf("\t\t\t\t    O computador vence por estar mais perto de 21.\n");
                    pc_wins();
                } else { 													//a ultima op��o seria o jogador tem mais pontos que o pc
                    printf("\t\t\t\t    O jogador vence por estar mais perto de 21.\n");
                    player_wins();
                }
                printf("\n");
                system("pause");
                break;


            case '2': // JOGADOR x JOGADOR (mais conhecido como X1)
                
				//Vez do jogador numero 1.
				opjogo = 'S';                                          		 // faz com que entre diretamente no while
                
				system("cls");
                cabecalho();
                
				printf("\t\t\t\tO jogo funciona por turnos, cada jogador tera sua vez de jogar\n");  //print explicando as como ser� o game
                printf("\t\t\t\t Voc� tem que fazer o mais pr�ximo de 21 pontos para ganhar...\n");
                printf("\t\t\t\t\t   Por�m n�o pode passar desse limite\n");
                printf("\t\t\t\t\t    Voc� pode parar ap�s cada jogada\n\n\n");
                
				total1 = 0; 												// inicializando a vari�vel do jogador 1
                
				while ((opjogo == 's') || (opjogo == 'S')) 					//Laco que deixa as jogadas a criterio do usuario.
                {    
					printf("\t\t\t\t\t  JOGADOR 1 deseja fazer a jogada?[S/N]\n");
                    scanf("%c", &opjogo); 								    //recebe se o usuario quer fazer a jogada, ou n�o
                    getchar();
                    printf("\n");
                    
					if ((opjogo == 'n') || (opjogo == 'N')) {				//se for um "n/N", o programa para. Se ele quiser continuar...
                        break;
                    }
                    
					jogada = mostrar_carta();								//fun��o que gera um n�mero aleat�rio, e mostra sua respectiva carta
                    
					total1 += jogada; 									    // o total do jogador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
                    
					printf("\t\t\t\t\t\t      PONTOS: %d", total1);
                    linha(); 											    // printa uma linha de "#" (funcao do arquivo 'cabecalho.c')
                    
					if (total1 >= 21) {									    //se o valor passar dos 21, ele n�o pode pedir mais cartas.
                        break;
                    }
                }
                
				printf("\t\t\t\t\t\tJOGADOR 1 MARCOU %d PONTOS", total1);	// mostra o total que o jogador conseguiu juntar e continua pra vez do COMPUTADOR
                linha();
                system("pause");

                
				//Vez do jogador numero 2.
                
				opjogo = 'S';                                          		 // faz com que entre diretamente no while
                
				system("cls");
                cabecalho();
                printf("\t\t\t\t\t      Agora � o turno do jogador 2\n");  	//print explicando as como ser� o game
                printf("\t\t\t\t Voc� tem que fazer o mais pr�ximo de 21 pontos para ganhar...\n");
                printf("\t\t\t\t\t   Por�m n�o pode passar desse limite\n");
                printf("\t\t\t\t\t    Voc� pode parar ap�s cada jogada\n\n\n");
                
				total2 = 0; 												// inicializando a vari�vel do jogador 1
                
				while ((opjogo == 's') || (opjogo == 'S')) 					//Laco que deixa as jogadas a criterio do usuario.
                {   
					printf("\t\t\t\t\t  JOGADOR 2 deseja fazer a jogada?[S/N]\n");
                    scanf("%c", &opjogo); 								    //recebe se o usuario quer fazer a jogada, ou n�o
                    getchar();
                    printf("\n");
                    
					if ((opjogo == 'n') || (opjogo == 'N')) {				//se for um "n/N", o programa para. Se ele quiser continuar...
                        break;
                    }
                    
					jogada = mostrar_carta();								//fun��o que gera um n�mero aleat�rio, e mostra sua respectiva carta
                    
					total2 += jogada; 									    // o total do jogador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
                    
					printf("\t\t\t\t\t\t      PONTOS: %d", total2);
                    linha(); 											    // printa uma linha de "#" (funcao do arquivo 'cabecalho.c')
                    
					if (total2 >= 21) {									    //se o valor passar dos 21, ele n�o pode pedir mais cartas.
                        break;
                    }
                }
               
			    printf("\t\t\t\t\t\tJOGADOR 2 MARCOU %d PONTOS", total2);	// mostra o total que o jogador conseguiu juntar e continua pra vez do COMPUTADOR
                linha();
                
				printf("\t\t\t\t\t\t\tPLACAR\n");
                printf("\t\t\t\t\t   JOGADOR 1  |%d| x |%d|  JOGADOR 2", total1, total2);
                linha();

                
				//Condi��es para final do jogo
                if (total1 == total2) {  									//se os dois placares for iguais
                    printf("\t\t\t\t\t\tHouve um empate...\n\n");
                    empate();
                } else if (total1 == 21) { 									//se o jogador1 fez 21 pontos exatos e o jogador2 nao
                    printf("\t\t\t\t\tO JOGADOR 1 GANHOU, fazendo os gloriosos 21 pontos...\n");
                    player_wins();
                } else if (total2 == 21) {							 		//se o jogador2 fez 21 pontos exatos e o jogador1 nao
                    printf("\t\t\t\tO JOGADOR 2 GANHOU, fazendo os gloriosos 21 pontos...\n");
                    player_wins();
                } else if ((total1 > 21) && (total2 > 21)) { 				//se o jogador1 e o jogador2 ultrapassaram os 21 pontos
                    printf("\t\t\t\t    O JOGADOR 1 e o JOGADOR 2 perdem por terem um n�mero maior de pontos do que � permitido...\n");
                } else if (total1 > 21 && total2 < 21) { 					//se o jogador1 ultrapassou os 21 pontos, e o jogador2 n�o ultrapassou
                    printf("\t\t\t   O JOGADOR 1 tem um n�mero maior de pontos do que � permitido...\n\t\t\t\t\t\tO JOGADOR 2 VENCE\n");
                    player_wins();
                } else if (total2 > 21 && total1 < 21) { 					//se o jogador2 ultrapassou os 21 pontos, e o jogador1 n�o ultrapassou
                    printf("\t\t\t   O JOGADOR 2 tem um n�mero maior de pontos do que � permitido...\nt\t\t\t\t\t\tO JOGADOR 1 VENCE\n");
                    player_wins();
                } else if (total1 > total2) { 					        	//se o jogador1 tem mais pontos que o jogador2
                    printf("\t\t\t\t    O JOGADOR 1 vence por estar mais perto de 21.\n");
                    player_wins();
                } else { 													//a ultima op��o seria o jogador2 tem mais pontos que o jogador1
                    printf("\t\t\t\t    O JOGADOR 2 vence por estar mais perto de 21.\n");
                    player_wins();
                }
                printf("\n");
                system("pause");
                break;
            
			case '3':														//menu regras
                system("cls");
                regras();
                system("pause");
                break;
            
			case '4':  														//menu creditos
                system("cls");
                creditos();
                system("pause");
                break;
            
			case '5': 														//encerrar programa
                system("cls");
                fimdejogo();
                system("pause");
                
            default:  														//programa��o segura
            	system("cls");
            	cabecalho();
            	printf("\n\n\n\t\t\t\t\tOp��o inv�lida, tente novamente...\n\n\n");
            	system("pause");
            	
        }
        
        system("cls");
        
    } while(opjogo != '5');

    return 0;
}
